#!/bin/bash
#tinygo build -o main.wasm -target wasm ./main.go

tinygo build -wasm-abi=generic -target=wasi -o main.wasm main.go

ls -lh *.wasm